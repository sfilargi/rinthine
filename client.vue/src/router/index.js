import Vue from 'vue'
import Router from 'vue-router'
import Login from '@/components/Login'
import Main from '@/components/Main'
import Home from '@/components/Home'
import { store } from '../model'

Vue.use(Router)

const router = new Router({
  mode: 'history',
  routes: [
    { path: '/login', name: 'Login', component: Login },
    {
      path: '/', component: Main, meta: { auth: true },
      children: [
        { path: '/', name: 'Home', component: Home, meta: {auth: true} }
      ]
    }
  ]
})

router.beforeEach((to, from, next) => {
  if (to.matched.some(record => record.meta.auth) && !store.state.token) {
    next({ path: '/login' })
  } else if (to.matched.some(record => !record.meta.auth) && store.state.token) {
    next({ path: '/' })
  } else {
    next()
  }
})

export default router
