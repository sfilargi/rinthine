import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

var file_id = 1

export const store = new Vuex.Store({
    state: {
        token: localStorage.getItem('token'),
        files: {},
        queue: [],
        in_progress: 0,
    },
    mutations: {
        setToken(state, token) {
            state.token = token
        },
        test(state) {
            console.log('during')
        },
        in_progress_decrease(state) {
            state.in_progress -= 1
        },
        addFile(state, file) {
            state.files[file.id] = file
            state.queue.push(file.id)
        },
        dequeue(state) {
            state.queue.shift()
            state.in_progress += 1
        }
    },
    actions: {
        addFile(ctx, file) {
            file_id += 1
            ctx.commit('addFile', {
                id: file_id,
                file: file,
            })
            ctx.dispatch('dequeue')
        },
        dequeue(ctx) {
            if (ctx.state.in_progress || ctx.state.queue.length == 0) {
                return
            }
            const next = ctx.state.queue[0]
            const file = ctx.state.files[next]
            ctx.commit('dequeue')
            setTimeout(() => {
                ctx.commit('in_progress_decrease')
                ctx.dispatch('dequeue')
            }, 5000)
        }
    }
})
