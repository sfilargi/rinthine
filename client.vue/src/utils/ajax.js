import { store } from '../model'

function fetch_internal(url, method, data, cb) {
    fetch(url, {
        credentials: "same-origin",
        headers: {
            'Content-Type': 'application/json',
            'X-Rinthin-Token': store.state.token,
        },
        method: method,
        body: data ? JSON.stringify(data) : null,
    }).then(function (response) {
        response.json().then(function (data) {
            cb(response.status, data)
        }).catch(function () {
            cb(response.status, null)
        })
    }).catch(function () {
        cb(0, null)
    })
}

export function get(url, cb) {
    return fetch_internal(url, 'GET', null, cb)
}

export function post(url, data, cb) {
    return fetch_internal(url, 'POST', data, cb)
}