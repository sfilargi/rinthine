"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const Koa = require("koa");
const Router = require("koa-router");
const photos = require("./controllers/photos");
const users = require("./controllers/users");
const app = new Koa();
const router = new Router();
router.use('/api/photos', photos.routes);
router.use('/api/users', users.routes);
app.use(async function (ctx, next) {
    try {
        await next();
        console.log(`${ctx.response.status} ${ctx.request.method} ${ctx.request.path}`);
    }
    catch (e) {
        console.log(e);
        ctx.response.status = 500;
        ctx.response.body = 'Unknown Error';
    }
});
app.use(router.routes());
app.listen(3000);
//# sourceMappingURL=index.js.map