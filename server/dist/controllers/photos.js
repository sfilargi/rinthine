"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const Router = require("koa-router");
const fs = require("fs");
const crypto = require("crypto");
const path = require("path");
const photo_1 = require("../models/photo");
const authenticate_1 = require("../utils/authenticate");
const router = new Router();
async function saveBody(ctx) {
    const randomId = await crypto.randomBytes(32).toString('hex');
    const filename = path.resolve('tmp', randomId);
    return new Promise(function (resolve, reject) {
        const file = fs.createWriteStream(filename);
        ctx.req.pipe(file);
        file.on('finish', function () {
            resolve(filename);
        });
    });
}
async function del(file) {
    return new Promise(function (resolve) {
        fs.unlink(file, function () {
            resolve();
        });
    });
}
router.post('/:filename', authenticate_1.default, async function (ctx) {
    const local_filename = await saveBody(ctx);
    try {
        const photo = await photo_1.default.create(ctx.state.user, local_filename);
        if (photo) {
            ctx.body = { id: photo.id };
        }
        else {
            ctx.status = 500;
            ctx.body = 'duplicate';
        }
    }
    finally {
        del(local_filename);
    }
});
exports.routes = router.routes();
//# sourceMappingURL=photos.js.map