"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const Router = require("koa-router");
const body_1 = require("../utils/body");
const json_1 = require("../utils/json");
const login_token_1 = require("../models/login_token");
const auth_token_1 = require("../models/auth_token");
const router = new Router();
router.post('/login', body_1.default, json_1.default, async function (ctx) {
    if (typeof ctx.request.body.email != 'string') {
        throw 'email not a string';
    }
    const login_token = await login_token_1.default.create(ctx.request.body.email);
    ctx.body = login_token.token;
});
router.post('/authenticate', body_1.default, json_1.default, async function (ctx) {
    const data = ctx.request.body;
    if (typeof data.email != 'string') {
        throw 'email not a string';
    }
    if (typeof data.token != 'string') {
        throw 'token not a string';
    }
    const login_token = await login_token_1.default.find(data.email, data.token);
    if (!login_token) {
        throw 'authentication';
    }
    login_token.delete();
    const auth_token = await auth_token_1.default.create(data.email);
    ctx.body = { token: auth_token.token };
});
exports.routes = router.routes();
//# sourceMappingURL=users.js.map