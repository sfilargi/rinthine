"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const crypto = require("crypto");
const db_1 = require("./db");
db_1.default('auth_tokens').then((collection) => {
    collection.createIndex({ token: 1 });
});
async function collection() {
    return await db_1.default('auth_tokens');
}
class AuthToken {
    constructor(obj) {
        this._obj = obj;
    }
    static async create(email) {
        const tokens = await collection();
        const obj = {
            email,
            token: await crypto.randomBytes(64).toString('hex'),
            date: Date.now(),
        };
        const result = await tokens.insertOne(obj);
        obj._id = result.insertedId;
        return new AuthToken(obj);
    }
    static async find(token) {
        const tokens = await collection();
        const obj = await tokens.findOne({ token });
        if (!obj) {
            return null;
        }
        return new AuthToken(obj);
    }
    get email() {
        return this._obj.email;
    }
    get token() {
        return this._obj.token;
    }
}
exports.default = AuthToken;
//# sourceMappingURL=auth_token.js.map