"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const db_1 = require("./db");
db_1.default('users').then((collection) => {
    collection.createIndex({ email: 1 });
});
async function collection() {
    return await db_1.default('users');
}
class User {
    constructor(obj) {
        this._obj = obj;
    }
    static async findOrCreate(email) {
        const users = await collection();
        email = email.toLowerCase();
        let obj = await users.findOne({ email });
        if (obj) {
            return new User(obj);
        }
        obj = {
            email,
            created_at: Date.now(),
        };
        const result = await users.insertOne(obj);
        obj._id = result.insertedId;
        return new User(obj);
    }
    get email() {
        return this._obj.email;
    }
    get id() {
        return this._obj._id;
    }
}
exports.default = User;
//# sourceMappingURL=user.js.map