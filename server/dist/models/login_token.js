"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const crypto = require("crypto");
const db_1 = require("./db");
db_1.default('login_tokens').then((collection) => {
    collection.createIndex({ email: 1, token: 1 });
});
async function collection() {
    return await db_1.default('login_tokens');
}
class LoginToken {
    constructor(obj) {
        this._obj = obj;
    }
    static async create(email) {
        const tokens = await collection();
        const obj = {
            email,
            token: await crypto.randomBytes(18).toString('hex'),
            date: Date.now(),
        };
        const result = await tokens.insertOne(obj);
        obj._id = result.insertedId;
        return new LoginToken(obj);
    }
    static async find(email, token) {
        const tokens = await collection();
        const obj = await tokens.findOne({ email, token });
        if (!obj) {
            return null;
        }
        return new LoginToken(obj);
    }
    async delete() {
        const tokens = await collection();
        await tokens.remove({ _id: this._obj._id });
    }
    get email() {
        return this._obj.email;
    }
    get token() {
        return this._obj.token;
    }
}
exports.default = LoginToken;
//# sourceMappingURL=login_token.js.map