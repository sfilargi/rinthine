"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongo = require("mongodb");
var db = null;
const url = 'mongodb://localhost:27017/rinthine';
async function default_1(collection) {
    if (!db) {
        db = await mongo.MongoClient.connect(url);
    }
    return db.collection(collection);
}
exports.default = default_1;
//# sourceMappingURL=mongo.js.map