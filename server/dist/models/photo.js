"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const crypto = require("crypto");
const fs = require("fs");
const gm = require("gm");
const path = require("path");
const db_1 = require("./db");
const s3 = require("../utils/s3");
const collectionName = 'photos';
db_1.default(collectionName).then((collection) => {
    collection.createIndex({ hash_digest: 1 });
    collection.createIndex({ user_id: 1 });
});
async function collection() {
    return await db_1.default(collectionName);
}
async function hash(filename) {
    return new Promise(function (resolve) {
        const file = fs.createReadStream(filename);
        const hash = crypto.createHash('sha256');
        hash.setEncoding('hex');
        file.on('end', function () {
            hash.end();
            resolve(hash.read().toString());
        });
        file.pipe(hash);
    });
}
async function resize(original, thumb, width, height) {
    return new Promise(function (resolve, reject) {
        gm(original).quality(85).strip().
            resize(width, height).autoOrient()
            .write(thumb, function (error) {
            if (error) {
                reject(error);
            }
            else {
                resolve();
            }
        });
    });
}
async function createPreviews(filename) {
    const thumbId = crypto.randomBytes(32).toString('hex');
    const thumbFilename = path.resolve('tmp', thumbId + '.jpg');
    await resize(filename, thumbFilename, '256>', '256>');
    const largeId = crypto.randomBytes(32).toString('hex');
    const largeFilename = path.resolve('tmp', largeId + '.jpg');
    await resize(filename, largeFilename, '1024>', '1024>');
    return {
        thumbFilename,
        largeFilename,
    };
}
async function del(file) {
    return new Promise(function (resolve) {
        fs.unlink(file, function () {
            resolve();
        });
    });
}
async function delPreviews(previews) {
    await del(previews.thumbFilename);
    await del(previews.largeFilename);
}
class Photo {
    constructor(obj) {
        this._obj = obj;
    }
    static async create(user, filename) {
        const photos = await collection();
        const hashDigest = await hash(filename);
        const exists = await photos.findOne({ hash_digest: hashDigest });
        if (exists) {
            return null;
        }
        const previews = await createPreviews(filename);
        const obj = {
            _id: await crypto.randomBytes(32).toString('hex'),
            user_id: user.id,
            hash_digest: hashDigest,
            original_id: await crypto.randomBytes(32).toString('hex'),
            thumb_id: await crypto.randomBytes(32).toString('hex'),
            large_id: await crypto.randomBytes(32).toString('hex'),
            created_at: Date.now(),
        };
        try {
            await s3.upload('rinthine', obj.original_id, filename);
            await s3.upload('rinthine', obj.large_id, previews.largeFilename);
            await s3.upload('rinthine', obj.thumb_id, previews.thumbFilename);
        }
        finally {
            await delPreviews(previews);
        }
        const result = await photos.insertOne(obj);
        return new Photo(obj);
    }
    get id() {
        return this._obj._id;
    }
}
exports.default = Photo;
//# sourceMappingURL=photo.js.map