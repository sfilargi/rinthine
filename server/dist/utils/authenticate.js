"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const auth_token_1 = require("../models/auth_token");
const user_1 = require("../models/user");
async function authenticate(ctx, next) {
    const token = ctx.headers['x-rinthine-token'];
    if (!token) {
        ctx.body = 'unauthenticated';
        ctx.response.status = 500;
        return;
    }
    const auth_token = await auth_token_1.default.find(token);
    if (!auth_token) {
        ctx.body = 'unauthenticated';
        ctx.response.status = 500;
        return;
    }
    ctx.state.user = await user_1.default.findOrCreate(auth_token.email);
    await next();
}
exports.default = authenticate;
//# sourceMappingURL=authenticate.js.map