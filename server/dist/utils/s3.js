"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const fs = require("fs");
const aws = require("aws-sdk");
const s3 = new aws.S3();
async function upload(bucket, key, filename) {
    const file = fs.createReadStream(filename);
    return new Promise(function (resolve, reject) {
        s3.upload({ Body: file, Bucket: bucket, Key: key }, function (error, reply) {
            if (error) {
                reject(error);
            }
            else {
                resolve();
            }
        });
    });
}
exports.upload = upload;
function download(bucket, key) {
    return s3.getObject({ Bucket: bucket, Key: key }).createReadStream();
}
exports.download = download;
//# sourceMappingURL=s3.js.map