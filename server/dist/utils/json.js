"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
async function json(ctx, next) {
    try {
        ctx.request.body = JSON.parse(ctx.request.body);
    }
    catch (e) {
        ctx.body = 'Not Json';
        ctx.response.status = 400;
        return;
    }
    await next();
}
exports.default = json;
//# sourceMappingURL=json.js.map