import * as Router from 'koa-router'

import body from '../utils/body'
import json from '../utils/json'

import LoginToken from '../models/login_token'
import AuthToken from '../models/auth_token'

const router = new Router()

router.post('/login', body, json, async function (ctx) {
    if (typeof ctx.request.body.email != 'string') {
        throw 'email not a string'
    }

    const login_token = await LoginToken.create(ctx.request.body.email)

    ctx.body = login_token.token
})

router.post('/authenticate', body, json, async function (ctx) {
    const data = ctx.request.body
    if (typeof data.email != 'string') {
        throw 'email not a string'
    }
    if (typeof data.token != 'string') {
        throw 'token not a string'
    }

    const login_token = await LoginToken.find(data.email, data.token)

    if (!login_token) {
       throw 'authentication'
    }

    login_token.delete()

    const auth_token = await AuthToken.create(data.email)

    ctx.body = {token: auth_token.token}
})

export const routes = router.routes() 