import * as Router from 'koa-router'
import * as fs from 'fs'
import * as crypto from 'crypto'
import * as path from 'path'

import Photo from '../models/photo'

import authenticate from '../utils/authenticate'

const router = new Router()

async function saveBody(ctx) {
    const randomId = await crypto.randomBytes(32).toString('hex')
    const filename = path.resolve('tmp', randomId)

    return new Promise<string>(function (resolve, reject) {
        const file = fs.createWriteStream(filename)

        ctx.req.pipe(file)

        file.on('finish', function () {
            resolve(filename)
        })
    })
}

async function del(file) {
    return new Promise(function (resolve) {
        fs.unlink(file, function() {
            resolve()
        })
    })
}

router.post('/:filename', authenticate, async function (ctx) {
    const local_filename = await saveBody(ctx)
    
    try {
        const photo = await Photo.create(ctx.state.user, local_filename)
        if (photo) {
            ctx.body = {id: photo.id}
        } else {
            ctx.status = 500
            ctx.body = 'duplicate'            
        }
    } finally {
        del(local_filename)
    }
})

export const routes = router.routes() 