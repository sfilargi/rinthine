import * as crypto from 'crypto'
import * as mongodb from 'mongodb'
import db from './db'

type ObjectType = {
    _id?: mongodb.ObjectID,
    email: string,
    created_at: number,
}

db('users').then((collection) => {
    collection.createIndex({email: 1})
})

async function collection() {
    return await db('users')
}

export default class User {
    _obj: ObjectType

    constructor(obj: ObjectType) {
        this._obj = obj
    }

    static async findOrCreate(email: string): Promise<User> {
        const users = await collection()

        email = email.toLowerCase()

        let obj: ObjectType|null = await users.findOne({email})

        if (obj) {
            return new User(obj)
        }
        
        obj = {
            email,
            created_at: Date.now(),
        }

        const result = await users.insertOne(obj)
        obj._id = result.insertedId

        return new User(obj)
    }

    get email() {
        return this._obj.email
    }

    get id() {
        return this._obj._id
    }
}