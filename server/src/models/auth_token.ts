import * as crypto from 'crypto'
import * as mongodb from 'mongodb'
import db from './db'

type ObjectType = {
    _id?: mongodb.ObjectID,
    email: string,
    token: string,
    date: number,
}

db('auth_tokens').then((collection) => {
    collection.createIndex({token: 1})
})

async function collection() {
    return await db('auth_tokens')
}

export default class AuthToken {
    _obj: ObjectType

    constructor(obj: ObjectType) {
        this._obj = obj
    }

    static async create(email: string): Promise<AuthToken> {
        const tokens = await collection()
        
        const obj: ObjectType = {
            email,
            token: await crypto.randomBytes(64).toString('hex'),
            date: Date.now(),
        }

        const result = await tokens.insertOne(obj)
        obj._id = result.insertedId

        return new AuthToken(obj)
    }

    static async find(token: string): Promise<AuthToken|null> {
        const tokens = await collection()

        const obj: ObjectType|null = await tokens.findOne({token})
        if (!obj) {
            return null
        }

        return new AuthToken(obj)
    }

    get email() {
        return this._obj.email
    }

    get token() {
        return this._obj.token
    }
}