import * as crypto from 'crypto'
import * as mongodb from 'mongodb'
import db from './db'

type ObjectType = {
    _id?: mongodb.ObjectID,
    email: string,
    token: string,
    date: number,
}

db('login_tokens').then((collection) => {
    collection.createIndex({email: 1, token: 1})
})

async function collection() {
    return await db('login_tokens')
}

export default class LoginToken {
    _obj: ObjectType

    constructor(obj: ObjectType) {
        this._obj = obj
    }

    static async create(email: string): Promise<LoginToken> {
        const tokens = await collection()
        
        const obj: ObjectType = {
            email,
            token: await crypto.randomBytes(18).toString('hex'),
            date: Date.now(),
        }

        const result = await tokens.insertOne(obj)
        obj._id = result.insertedId

        return new LoginToken(obj)
    }

    static async find(email: string, token: string): Promise<LoginToken|null> {
        const tokens = await collection()

        const obj: ObjectType|null = await tokens.findOne({email, token})
        if (!obj) {
            return null
        }

        return new LoginToken(obj)
    }

    async delete() {
        const tokens = await collection()

        await tokens.remove({_id: this._obj._id})
    }

    get email() {
        return this._obj.email
    }

    get token() {
        return this._obj.token
    }
}