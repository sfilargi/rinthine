import * as mongo from 'mongodb'

var db: mongo.Db = null

const url = 'mongodb://localhost:27017/rinthine';

export default async function (collection): Promise<mongo.Collection> {
    if (!db) {
        db = await mongo.MongoClient.connect(url)
    }

    return db.collection(collection)
}
