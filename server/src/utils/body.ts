import * as koa from 'koa'

export default async function body(ctx: koa.Context, next: () => Promise<any>): Promise<any> {
    const buffers = []
    var size = 0

    try {
        await new Promise(function (resolve, reject) {
            ctx.req.on('data', function (chunk) {
                size += chunk.length
                if (size > Math.pow(1024, 3)) {
                    reject()
                    return
                }
                buffers.push(chunk)
            })
            ctx.req.on('end', function () {
                ctx.request.body = Buffer.concat(buffers).toString()
                resolve()
                return
            })
        })
    } catch (e) {
        ctx.body = 'Body too long'
        ctx.response.status = 413
    }

    await next()
}