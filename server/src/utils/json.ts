import * as koa from 'koa'

export default async function json(ctx: koa.Context, next) {
    try {
        ctx.request.body = JSON.parse(ctx.request.body)
    } catch (e) {
        ctx.body = 'Not Json'
        ctx.response.status = 400
        return
    }
    await next()            
}