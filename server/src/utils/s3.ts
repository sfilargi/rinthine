import * as fs from 'fs'
import * as aws from 'aws-sdk'
const s3 = new aws.S3()

export async function upload(bucket, key, filename) {
  const file = fs.createReadStream(filename)
  return new Promise(function (resolve, reject) {
    s3.upload({ Body: file, Bucket: bucket, Key: key }, function (error, reply) {
      if (error) {
        reject(error)
      } else {
        resolve()
      }
    })
  })
}

export function download(bucket, key) {
  return s3.getObject({ Bucket: bucket, Key: key }).createReadStream()
}