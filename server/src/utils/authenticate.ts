import * as koa from 'koa'

import AuthToken from '../models/auth_token'
import User from '../models/user'

export default async function authenticate(ctx: koa.Context, next) {
    const token = ctx.headers['x-rinthine-token']
    if (!token) {
        ctx.body = 'unauthenticated'
        ctx.response.status = 500
        return
    }

    const auth_token = await AuthToken.find(token)
    if (!auth_token) {
        ctx.body = 'unauthenticated'
        ctx.response.status = 500
        return
    }

    ctx.state.user = await User.findOrCreate(auth_token.email)

    await next()            
}