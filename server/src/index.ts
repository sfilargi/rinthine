import * as fs from 'fs'

import * as Koa from 'koa'
import * as Router from 'koa-router'

import * as photos from './controllers/photos'
import * as users from './controllers/users'

const app = new Koa()

const router = new Router()

router.use('/api/photos', photos.routes)
router.use('/api/users', users.routes)

/*
 * Catch errors
 */
app.use(async function (ctx, next) {
    try {
        await next()
        console.log(`${ctx.response.status} ${ctx.request.method} ${ctx.request.path}`)
    } catch (e) {
        console.log(e)
        ctx.response.status = 500
        ctx.response.body = 'Unknown Error'
    }
})
app.use(router.routes())

app.listen(3000)
