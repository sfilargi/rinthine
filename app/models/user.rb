class User < ApplicationRecord
  has_many :photos
  validates :name, length: { in: 2..128 }, if: -> { name.present? }
  validates :username, presence: true, length: {in: 4..64},
    uniqueness: { case_sensitive: false }
  validates :password, length: { minimum: 8 }, if: -> { password.present? }
  has_secure_password
end
