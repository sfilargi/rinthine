class Blob
    attr_reader :id, :data
    class << self
      def create(data)
        id = SecureRandom.hex 32
        File.open('/photos/' + id, 'wb') do |file|
          file.write(data)
        end
        Blob.new id, data
      end
  
      def load(id)
        data = nil
        File.open('/photos/' + id, 'rb') do |file|
          data = file.read()
        end
        return nil unless data
        Blob.new id, data
      end
    end
  
    def initialize(id, data = nil)
      @id = id
      @data = data
    end
  end
  