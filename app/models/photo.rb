require 'mini_magick'
MiniMagick.configure do |config|
  config.cli = :graphicsmagick
  config.timeout = 5
end

def sanitize(data)
  name_in = './tmp/' + SecureRandom.hex
  name_out = './tmp/' + SecureRandom.hex + '.jpeg'
  file_in = File.new name_in, 'wb'
  file_in.write data
  file_in.close

  image = MiniMagick::Image.open(name_in)
  image.auto_orient
  image.format 'jpeg'
  image.strip
  image.write name_out

  file_out = File.open name_out, 'rb'
  transformed = file_out.read
  file_out.close
  File.unlink name_in
  File.unlink name_out
  transformed
end

def crop_avatar(data, x, y, width, height, rotation)
  name_in = './tmp/' + SecureRandom.hex
  name_out = './tmp/' + SecureRandom.hex + '.jpeg'
  file_in = File.new name_in, 'wb'
  file_in.write data
  file_in.close

  image = MiniMagick::Image.open(name_in)
  image.crop "#{width}x#{height}+#{x}+#{y}"
  image.rotate rotation
  image.resize '200x200'
  image.write name_out

  file_out = File.open name_out, 'rb'
  transformed = file_out.read
  file_out.close
  File.unlink name_in
  File.unlink name_out
  transformed
end

class Photo < ApplicationRecord
  belongs_to :user
  has_one :post

  def upload(data)
    original_blob = Blob.create data
    data = sanitize data
    fullsize_blob = Blob.create data

    self.original = original_blob.id
    self.fullsize = fullsize_blob.id
    save
  end

  def create_avatar(params)
    data = fullsize_blob
    avatar_data = crop_avatar data, params[:x], params[:y], params[:width],
                       params[:height], params[:rotation]
    avatar_blob = Blob.create avatar_data
    self.avatar = avatar_blob.id
    user.avatar = avatar_blob.id
    save
  end

  def fullsize_blob
    Blob.load(fullsize).data
  end

  def avatar_blob
    Blob.load(avatar).data
  end
end
