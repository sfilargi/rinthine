class Session
  include ActiveModel::Model

  attr_accessor :username, :password, :user

  validates :username, presence: true
  validates :password, presence: true

  def self.authenticate(params)
    session = Session.new params
    session.valid? && session.authenticate
    session
  end

  def authenticate
    user = User.find_by(username: username.downcase)
    unless user
      errors.add :username, 'doesn\'t exist'
      return
    end
    unless user.authenticate password
      errors.add :password, 'is wrong'
      return
    end
    @user = user
  end
end
