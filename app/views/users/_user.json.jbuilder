json.extract! user, :name, :username, :email
json.url user_url(user, format: :json)
