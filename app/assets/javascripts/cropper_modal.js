(function webpackUniversalModuleDefinition(root, factory) {
	if(typeof exports === 'object' && typeof module === 'object')
		module.exports = factory();
	else if(typeof define === 'function' && define.amd)
		define([], factory);
	else {
		var a = factory();
		for(var i in a) (typeof exports === 'object' ? exports : root)[i] = a[i];
	}
})(window, function() {
return /******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./src/index.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./src/index.js":
/*!**********************!*\
  !*** ./src/index.js ***!
  \**********************/
/*! exports provided: CropperModal */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"CropperModal\", function() { return CropperModal; });\nconst CropperModal = (function($) {\n    const NS = 'rin.CropperModal'\n    const template = __webpack_require__(/*! ./template.html */ \"./src/template.html\");\n\n    class CropperModal {\n        constructor (element, url) {\n            this._parent = element;\n            this._element = $(template);\n            $(this._parent).prepend(this._element);\n            this._image = $(this._element).find('.image')[0];\n            $(this._image).attr('src', url);\n            this._cropper = new Cropper(this._image, {\n                aspectRatio: 1 / 1,\n                crop: (event) => {\n                    this._details = event.detail;\n                },\n            });\n            this.success = function() {}\n            this.fail = function() {}\n            $(this._element).on('click', '#apply', (e) => {\n                e.preventDefault();\n                $(this._element).remove();\n                this.success(this._details);\n            });\n            $(this._element).on('click', '#cancel', (e) => {\n                e.preventDefault();\n                $(this._element).remove();\n                this.fail();\n            });\n            this._element.modal();\n        }\n    }\n\n    $.fn['CropperModal'] = function() {\n        return this.each(function () {\n            let data = $(this).data(NS);\n            if (!data) {\n                data = new CropperModal(this);\n                $(this).data(NS, data);\n            }\n        });\n    }\n\n    return CropperModal;\n})($);\n\n\n//# sourceURL=webpack:///./src/index.js?");

/***/ }),

/***/ "./src/template.html":
/*!***************************!*\
  !*** ./src/template.html ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("module.exports = \"<div class=\\\"modal fade\\\" tabindex=\\\"-1\\\" role=\\\"dialog\\\">\\n    <div class=\\\"modal-dialog modal-dialog-centered\\\" role=\\\"document\\\">\\n      <div class=\\\"modal-content\\\">\\n        <div class=\\\"modal-header\\\">\\n          <h5 class=\\\"modal-title\\\">Crop photo</h5>\\n          <button type=\\\"button\\\" class=\\\"close\\\" data-dismiss=\\\"modal\\\">\\n            <span>&times;</span>\\n          </button>\\n        </div>\\n        <div class=\\\"modal-body\\\">\\n            <div>\\n              <img class=\\\"image\\\" style=\\\"max-width: 400px\\\" src=\\\"\\\">\\n            </div>\\n        </div>\\n        <div class=\\\"modal-footer\\\">\\n          <button type=\\\"button\\\" id=\\\"apply\\\" class=\\\"btn btn-secondary\\\" data-dismiss=\\\"modal\\\">Close</button>\\n          <button type=\\\"button\\\" id=\\\"cancel\\\" class=\\\"btn btn-primary\\\">Apply</button>\\n        </div>\\n      </div>\\n    </div>\\n  </div>\"\n\n//# sourceURL=webpack:///./src/template.html?");

/***/ })

/******/ });
});