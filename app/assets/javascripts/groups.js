'use strict';
(function($) {
    const NS      = 'rin.user-select';
    const KEYDOWN = 40;
    const KEYUP   = 38;
    const ENTER   = 13;

    const reg  = function(...arg) {
        $(document).on(...arg);
    }

    function getData(el, name) {
        if (el.data(name) === undefined) {
            el.data(name, {});
        }
        return el.data(name);
    }

    reg(`${NS}.change`, '.select-wrap', function(e, value) {
        const el = $(this);
        const data = getData(el, NS);

        const dropdown = $(el.children('.select-dropdown')[0]);
        if (data.ajax && data.ajax.readState != 4) {
            data.ajax.abort();
        }
        dropdown.css('display', 'none');
        if (value != '') {
            data.ajax = this._ajax = $.ajax(`/users?q=${value}`, {
                headers: {          
                    Accept: "application/json",
                }
            })
            .success((users) => {
                dropdown.empty();
                users.forEach((user) => {
                    const item = $(`<div class="select-item">${user.name}</div>`);
                    item.data(NS, user);
                    dropdown.append(item);
                })
                dropdown.css('display', 'block');
            });
        }
    });

    reg(`${NS}.move`, '.select-wrap', function(ev, direction) {
        const old_active = $(this).find('.select-item.active').first();
        if (old_active.length) {
            const next = direction == KEYDOWN ? old_active.next().first() : old_active.prev().first()
            if (next.length) {
                $(old_active).removeClass('active');
                $(next).addClass('active');
            }
        } else {
            $(this).find('.select-item').first().addClass('active');
        }
    });

    reg(`${NS}.hover`, '.select-wrap', function(ev, item) {
        const old_active = $(this).find('.select-item.active').first();
        old_active.removeClass('active');
        $(item).addClass('active');
    });

    reg(`${NS}.selected`, '.select-wrap', function(ev, item) {
        const el = item ? $(item) : $(this).find('.select-item.active').first();
        console.log(el);
        if (el.length) {
            $(this).find('.select-input').val('');
            $(this).children('.select-dropdown').css('display', 'none');
        }
    });

    // Native event tranformers
    function selectInputChange(el, value) {
        const data = getData(el, NS);

        if (data.lastValue == value) {
            return;
        }
        data.lastValue = value;

        el.trigger(`${NS}.change`, value);
    }

    function keydown(el, ev) {
        switch (ev.keyCode) {
            case KEYDOWN:
            $(el).trigger(`${NS}.move`, KEYDOWN);
            break;
            case KEYUP:
            $(el).trigger(`${NS}.move`, KEYUP);
            break;
            case ENTER:
            $(el).trigger(`${NS}.selected`);
            break;
        }
    };
    
    reg('change', '.select-input', function(ev) { 
        selectInputChange($(this), ev.target.value);
    });

    reg('keyup', '.select-input', function(ev) {
        selectInputChange($(this), ev.target.value);
    });

    reg('mouseover', '.select-item', function(ev) {
        $(this).trigger(`${NS}.hover`, $(this));
    });

    reg('click', '.select-item', function(ev) {
        $(this).trigger(`${NS}.selected`, this);
    });

    reg('keydown', '.select-input', function(ev) {
        keydown(this, ev);
    });

    reg('keydown', '.select-item', function(ev) {
        keydown(this, ev);
    });
    
}($));