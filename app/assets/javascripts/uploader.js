'use strict';

(function($) {
    const NS = 'rin.uploader';

    $(document).on(`${NS}.file-upload-failed`, '.uploader-wrap', function() {
        $(this).find('.uploader-button').removeClass('loading');
    });

    $(document).on(`${NS}.file-created`, '.uploader-wrap', function(ev, photo) {
        const reader = new FileReader();
        reader.onload = () => {
            $.ajax(`/photos/${photo.id}`, {
                method: 'PUT',
                headers: {
                    'Content-Type': 'application/octet-stream'
                },
                processData: false,
                data: reader.result,
            }).success(() => {
                $(this).trigger(`${NS}.file-uploaded`);
                console.log(CropperModal);
                const modal = new CropperModal(this, `/photos/${photo.id}`);
                modal.success = (crop) => {
                    console.log('yah!', crop);
                };
            }).fail(() => {
                $(this).trigger(`${NS}.file-upload-failed`);
            });
        }
        reader.readAsArrayBuffer(photo.file);
    });

    $(document).on(`${NS}.file-selected`, '.uploader-wrap', function(ev, file) {
        $(this).find('.uploader-button').addClass('loading');
        $.ajax('/photos', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                name: file.name,
                mimetype: file.type,
            }),
        }).success((photo) => {
            $(this).trigger(`${NS}.file-created`, {id: photo.id, file: file})
        }).fail(() => {
            $(this).trigger(`${NS}.file-upload-failed`);
        });
    });

    $(document).on('change', '.uploader-input', function(ev) {
        console.log(ev);
        $(this).trigger(`${NS}.file-selected`, ev.target.files[0]);
    });

}($));