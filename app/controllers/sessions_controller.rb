class SessionsController < ApplicationController
  before_action :set_session, only: [:show, :edit, :update, :destroy]
  # GET /sessions/new
  def new
    @session = Session.new
  end

  # POST /sessions
  # POST /sessions.json
  def create
    @session = Session.authenticate session_params

    respond_to do |format|
      if @session.user.nil?
        format.js { render :new, status: 403 }
        format.html { render :new }
      else
        log_in @session.user
        format.html { redirect_to :root }
      end
    end
  end

  private

  def session_params
    params.permit(:username, :password)
  end
end
