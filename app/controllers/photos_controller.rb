class PhotosController < ApplicationController
    before_action :logged_in_user

    def create
        @photo = current_user.photos.create photo_params
        render json: { id: @photo.id }
    end

    def update
        photo = current_user.photos.find(params[:id])
        photo.upload(request.body.read)
    end

    def show
        # todo: fixme: authorization
        photo = Photo.find(params[:id])
        send_data photo.fullsize_blob, type: 'image/jpeg'
    end

    private
    def photo_params
        params.permit(:mimetype, :name)
    end
end
