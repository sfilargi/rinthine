class UsersController < ApplicationController
  # GET /users/new
  def new
    @user = User.new
  end

  # POST /users
  # POST /users.json
  def create
    @user = User.new(user_params)

    respond_to do |format|
      if @user.save
        log_in @user
        format.html { redirect_to :root, notice: 'User was successfully created.' }
        format.json { redirect_to :root, status: :created, location: @user }
      else
        format.js { render :new }
        format.html { render :new }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  def index
    respond_to do |format|
      format.js { render :new }
      format.json { render json: ['stavros', 'nikos'] }
    end
  end

  def show
    respond_to do |format|
      format.js { render :new }
      format.json { render json: [{name: params['q'], id: 1}, {name: 'stavros', id: 2}, {name: 'nikos', id: 3}] }
    end
  end

  def edit
    @user = current_user
  end

  private
    def user_params
      params.require(:user).permit(:name, :username, :password, :email)
    end
end
