class GroupsController < ApplicationController
    before_action :logged_in_user
    def new
        @group = Group.new
    end
end
