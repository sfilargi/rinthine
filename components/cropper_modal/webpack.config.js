const path = require('path');

module.exports = {
  entry: './src/index.js',
  mode: 'development',
  output: {
    filename: 'cropper_modal.js',
    path: path.resolve(__dirname, '../../app/assets/javascripts/'),
    libraryTarget: 'umd',
  },
  module: {
  rules: [
      {
        test: /\.html$/,
        use: 'raw-loader'
      }
    ]
  },
};
