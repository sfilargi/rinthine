export const CropperModal = (function($) {
    const NS = 'rin.CropperModal'
    const template = require('./template.html');

    class CropperModal {
        constructor (element, url) {
            this._parent = element;
            this._element = $(template);
            $(this._parent).prepend(this._element);
            this._image = $(this._element).find('.image')[0];
            $(this._image).attr('src', url);
            this._cropper = new Cropper(this._image, {
                aspectRatio: 1 / 1,
                crop: (event) => {
                    this._details = event.detail;
                },
            });
            this.success = function() {}
            this.fail = function() {}
            $(this._element).on('click', '#apply', (e) => {
                e.preventDefault();
                $(this._element).remove();
                this.success(this._details);
            });
            $(this._element).on('click', '#cancel', (e) => {
                e.preventDefault();
                $(this._element).remove();
                this.fail();
            });
            this._element.modal();
        }
    }

    $.fn['CropperModal'] = function() {
        return this.each(function () {
            let data = $(this).data(NS);
            if (!data) {
                data = new CropperModal(this);
                $(this).data(NS, data);
            }
        });
    }

    return CropperModal;
})($);
