FROM ruby:2.5
RUN apt-get update -qq 
RUN curl -sL https://deb.nodesource.com/setup_8.x | bash -
RUN apt-get install -y build-essential nodejs mariadb-client libmariadb-dev
RUN apt-get install -y graphicsmagick
RUN mkdir /photos
RUN mkdir /rinthine
WORKDIR /rinthine
COPY Gemfile /rinthine/Gemfile
COPY Gemfile.lock /rinthine/Gemfile.lock
RUN bundle install
COPY . /rinthine