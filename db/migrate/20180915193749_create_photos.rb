class CreatePhotos < ActiveRecord::Migration[5.2]
  def change
    create_table :photos do |t|
      t.integer :user_id
      t.string :name
      t.string :mimetype
      t.text :metadata
      t.boolean :is_published
      t.string :original
      t.string :fullsize
      t.string :avatar
      t.string :hvga
      t.string :hd

      t.timestamps
    end
  end
end
