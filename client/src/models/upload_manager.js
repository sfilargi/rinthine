import { store, actions } from '../store'

var _nextId = 1

class File {
    constructor(file) {
        this._file = file
        this._state = 'pending'
        this._id = _nextId++
        this._progress = 0
        this.updateStore()
    }

    updateStore() {
        store.dispatch(actions.updateFile({
            id: this.id,
            file: this.file.name,
            state: this.state,
            progress: this.progress,
        }))
    }

    upload(failure, success, progress) {
        const reader = new FileReader()
        const req = new XMLHttpRequest()

        req.open('POST', `/api/photos/${this._file.name}`)
        req.setRequestHeader('X-Rinthine-Token', store.getState().token)
        req.setRequestHeader('Content-Type', 'application/octet-stream')
        req.upload.onprogress = (e) => {
            this._progress = e.loaded / e.total
            if (progress) {
                this.updateStore()
            }
        }
        req.onload = (e) => {
            if (e.target.status == 200) {
                console.log('Success', this.id)
                this._state = 'done'
                this.updateStore()
                success()
            } else {
                console.log('Failure', this.id)
                this._state = 'failed'
                this.updateStore()
                failure()
            }
        }
        req.onerror = (e) => {
            console.log('Failure', this.id)
            this._state = 'failed'
            this.updateStore()
            failure()
        }

        req.send(this._file)

        this._state = 'uploading'
        this.updateStore()
    }

    get file() {
        return this._file
    }

    get state() {
        return this._state
    }

    get name() {
        return this._file.name
    }

    get id() {
        return this._id
    }

    get progress() {
        return this._progress
    }
}

class UploadManager {
    constructor() {
        this._files = []
        this._queue = []
        this._in_progress = 0
        this._concurrency = 1
    }

    _upload_start(file) {
        this._in_progress++
        file.upload(
            () => {
                this._in_progress--
                this._dequeue()
            },
            () => {
                this._in_progress--
                this._dequeue()
            },
            () => {
            },
        )
    }

    _dequeue() {
        if (this._queue.length === 0) {
            return
        }
        const file = this._queue.shift()
        this._upload_start(file)
    }

    enqueue(file) {
        this._files.push(file)
        if (this._in_progress < this._concurrency) {
            this._upload_start(file)
        } else {
            this._queue.push(file)
        }
    }

    upload(files) {
        for (var i = 0; i < files.length; i++) {
            const file = new File(files[i])
            this.enqueue(file)
        }
    }

    get files() {
        return this._files
    }
}

export default new UploadManager()
