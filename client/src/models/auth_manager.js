import { store, actions } from '../store'

class AuthManager {
    login(token) {
        localStorage.setItem('token', token)
        store.dispatch(actions.setToken(token))
    }
}

export default new AuthManager()