import React from 'react'
import { Provider, connect } from 'react-redux'
import { BrowserRouter, Route, Link } from 'react-router-dom'

import { store } from './store'

import Main from './views/main.jsx'

export class App extends React.Component {
  render() {
    return (
      <BrowserRouter>
        <Provider store={store}>
          <Main />
        </Provider>
      </BrowserRouter>
    )
  }
}

