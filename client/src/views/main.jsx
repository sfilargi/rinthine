import { connect } from 'react-redux'

import Login from './login.jsx'
import Layout from './layout.jsx'

const Main = function(props) {
  if (props.authenticated) {
    return <Layout />
  } else {
    return <Login />
  }
}

const mapStateToProps = state => {
  return {
    authenticated: state.authenticated
  }
}

export default connect(mapStateToProps)(Main)