import React from 'react'
import { connect } from 'react-redux'
import { Route, Link } from 'react-router-dom'
import uploadManager from '../models/upload_manager'

const Home = function () {
  return (
    <div>
      Home
    </div>
  )
}

class FileUploader_ extends React.Component {
  render() {
    return (
      <div className="file">
        <label className="file-label">
          <input className="file-input" type="file" name="resume" multiple onChange={this.uploadFiles.bind(this)} />
          <span className="file-cta">
            <span className="file-icon">
              <i className="fa fa-upload"></i>
            </span>
            <span className="file-label">
              Upload {this.props.file_count}
            </span>
          </span>
        </label>
      </div>
    )
  }
  uploadFiles(e) {
    uploadManager.upload(e.target.files)
    e.target.value = null
  }
}

const FileUploader = connect(state => ({
  file_count: Object.keys(state.files).reduce((total, file) => (total + 1), 0)
}))(FileUploader_)

export default function () {
  return (
    <div className="main">
      <nav className="navbar has-shadow">
        <div className="container">
          <div className="navbar-brand">
            <Link className="navbar-item" to="/">
              Rinthine
            </Link>
          </div>
          <div className="navbar-end">
            <div className="navbar-item">
              <FileUploader />
            </div>
          </div>
        </div>
      </nav>
      <Route path="home" component={Home} />
    </div>
  )
}