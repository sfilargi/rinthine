import React from 'react'
import { connect } from 'react-redux'
import * as ajax from '../utils/ajax'
import { actions } from '../store'
import authManager from '../models/auth_manager'

class EmailForm extends React.Component {
  render() {
    return (
      <form onSubmit={this.submit.bind(this)}>
        <div className="field">
          <label className="label">Email</label>
          <div className="control">
            <input className="input" type="email" value={this.state.email}
              onChange={this.emailChange.bind(this)} />
          </div>
        </div>
        <div className="field">
          <div className="control">
            <button className="button is-primary">Next</button>
          </div>
        </div>
      </form>
    )
  }

  constructor() {
    super()
    this.state = {
      email: ''
    }
  }

  emailChange(e) {
    this.setState({
      email: e.target.value
    })
  }

  submit(e) {
    e.preventDefault()
    const email = this.state.email
    ajax.post('/api/users/login', {
      email
    }, (status_code, data) => {
      if (status_code == 200) {
        this.props.onSuccess(email)
      }
      //fixme
      this.props.onSuccess(email)
    })
  }
}

class TokenForm extends React.Component {
  render() {
    return (
      <form onSubmit={this.submit.bind(this)}>
        <div className="field">
          <label className="label">Token</label>
          <div className="control">
            <input className="input" type="string" value={this.state.token}
              onChange={this.tokenChange.bind(this)} />
          </div>
        </div>
        <div className="field">
          <div className="control">
            <button className="button is-primary">Login</button>
          </div>
        </div>
      </form>
    )
  }

  constructor() {
    super()
    this.state = {
      token: ''
    }
  }

  tokenChange(e) {
    this.setState({
      token: e.target.value
    })
  }

  submit(e) {
    e.preventDefault()
    console.log('submitting...')
    const token = this.state.token
    ajax.post('/api/users/authenticate', {
      email: this.props.email,
      token,
    }, (status_code, data) => {
      if (status_code == 200) {
        this.props.onSuccess(data.token)
      }
    })
  }
}

export default class Login extends React.Component {
  render() {
    let step
    if (!this.state.email) {
      step = <EmailForm onSuccess={this.emailSuccess.bind(this)} />
    } else {
      step = <TokenForm email={this.state.email} onSuccess={this.tokenSuccess.bind(this)} />
    }
    return (
      <div className="container">
        {step}
      </div>
    )
  }

  constructor() {
    super()
    this.state = {
      email: null
    }
  }

  emailSuccess(email) {
    this.setState({
      email
    })
  }

  tokenSuccess(token) {
    authManager.login(token)
  }
}