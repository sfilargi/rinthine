import React from 'react'
import ReactDOM from 'react-dom'

import { App } from './app.jsx'

import './assets/index.scss'

ReactDOM.render(React.createElement(App), document.getElementById('app'))