import { createStore } from 'redux'

const SET_TOKEN = 'setToken'
const UPDATE_FILE = 'updateFile'

function rootReducer(state, action) {
  if (typeof state === 'undefined') {
    return {
      authenticated: !!localStorage.getItem('token'),
      token: localStorage.getItem('token'),
      files: {},
    }
  }
  switch (action.type) {
    case SET_TOKEN:
      return {
        authenticated: true,
        token: action.data,
        files: state.files,
      }
    case UPDATE_FILE:
      const files = {}
      files[action.data.id] = action.data
      return Object.assign({}, state, {
        files: Object.assign({}, state.files, files)
      })
    default:
      return state
  }
}

export const actions = {
  setToken: (token) => ({
    type: SET_TOKEN,
    data: token
  }),
  updateFile: (file) => ({
    type: UPDATE_FILE,
    data: file,
  })
}

export const store = createStore(
  rootReducer,
  window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
)
