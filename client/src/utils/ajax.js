function fetch_internal(url, method, data, cb) {
    const response = fetch(url, {
        credentials: "same-origin",
        headers: {
            'Content-Type': 'application/json',
            'X-Rinthin-Token': null,
        },
        method: method,
        body: data ? JSON.stringify(data) : null,
    }).then(function(response) {
        response.json().then(function (data) {
            cb(response.status, data)
        }).catch(function(e) {
            console.log(e)
            cb(0, null)
        })
    }).catch(function(e) {
        console.log(e)
        cb(0, null)
    })
}

export function get(url, cb) {
    return fetch_internal(url, 'GET', null, cb)
}

export function post(url, data, cb) {
    return fetch_internal(url, 'POST', data, cb)
}